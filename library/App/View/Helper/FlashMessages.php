<?php
class App_View_Helper_FlashMessages extends Zend_View_Helper_Abstract
{
    /**
     * @return string
     */
    public function flashMessages()
    {
        $flashMessenger = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
        $messages = $flashMessenger->getMessages();
        if (empty($messages)) {
            $messages = $flashMessenger->getCurrentMessages();
            $flashMessenger->clearCurrentMessages();
            $flashMessenger->clearMessages();
        }
        if (!empty($messages)) {
            $output = '<ul id="flash-messages">';
            foreach ($messages as $message) {
                $output .= '<li class="alert alert-block alert-' . $message['type'] . '">' . $this->view->translate($message['content'])
                    . '</li>';
            }
            return $output . '</ul>';
        }
        return '';
    }

}
