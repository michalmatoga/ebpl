<?php
defined('APPLICATION_PATH')
|| define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/application'));

defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV')
    ? getenv('APPLICATION_ENV') : 'production'));

set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/* caching config files only if APPLICATION_ENV is set to production */
require_once 'Zend/Cache.php';
require_once 'Zend/Config/Ini.php';

$appIniPath = APPLICATION_PATH . '/configs/application.ini';
$navPath = APPLICATION_PATH . '/configs/navigation.php';
$routePath = APPLICATION_PATH . '/configs/routes.php';

$cache = Zend_Cache::factory('File', 'File', array(
    'master_files' => array($appIniPath, $navPath, $routePath),
    'automatic_serialization' => true,
    'caching' => (APPLICATION_ENV == 'production')),
array('cache_dir' => APPLICATION_PATH . '/../data/_cache')
);

if (!($config = $cache->load('application_conf'))) {
    $config = new Zend_Config_Ini($appIniPath, APPLICATION_ENV,
        array('allowModifications' => true));
    $config->merge(new Zend_Config(require $navPath))
        ->merge(new Zend_Config(require $routePath));
    $cache->save($config->toArray(), 'application_conf');
}
require_once 'Zend/Application.php';


$application = new Zend_Application(
    APPLICATION_ENV,
    $config
);

$application->bootstrap()->run();
