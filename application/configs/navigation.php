<?php
return array(
    'resources' => array(
        'navigation' => array(
            'pages' => array(
                array(
                    'label'      => 'Dostawca',
                    'controller' => 'supplier',
                    'action'     => 'errands',
                    'route'      => 'supplier-errands-errands',
                    'pages' => array(
                        array(
                            'label' => 'Nowe zlecenie',
                            'action'     => 'new',
                            'route'      => 'supplier-errands-new',
                        ), 
                        array(
                            'label' => 'Zlecenia',
                            'action'     => 'errands',
                            'route'      => 'supplier-errands-errands',
                        ) 
                    )
                ),
                array(
                    'label'      => 'Przewoźnik',
                    'controller' => 'carrier',
                    'action'     => 'index',
                    'route'      => 'carrier-index',
                    'pages' => array(
                        array(
                            'label' => 'Twoje oferty',
                            'action'     => 'bids',
                            'route'      => 'carrier-bids',
                        ), 
                        array(
                            'label' => 'Giełda zleceń',
                            'action'     => 'index',
                            'route'      => 'carrier-index',
                        ) 
                    )
                )
            )
        )
    )
);
