<?php
return array(
    'resources' => array(
        'router' => array(
            'routes' => array(
                'supplier' => array(
                    'type' => 'Zend_Controller_Router_Route_Static',
                    'route' => 'supplier',
                    'chains' => array(
                        'errands' => array(
                            'type' => 'Zend_Controller_Router_Route_Static',
                            'route' => 'errands',
                            'chains' => array(
                                'acceptbid' => array(
                                    'type' => 'Zend_Controller_Router_Route',
                                    'route' => 'akceptuj-oferte/:errandid/:id',
                                    'defaults' => array(
                                        'controller' => 'supplier',
                                        'action' => 'accept-bid'
                                    )
                                ), 
                                'show' => array(
                                    'type' => 'Zend_Controller_Router_Route',
                                    'route' => ':id',
                                    'defaults' => array(
                                        'controller' => 'supplier',
                                        'action' => 'show'
                                    )
                                ), 
                                'new' => array(
                                    'type' => 'Zend_Controller_Router_Route_Static',
                                    'route' => 'new',
                                    'defaults' => array(
                                        'controller' => 'supplier',
                                        'action' => 'new'
                                    )
                                ), 
                                'errands' => array(
                                    'type' => 'Zend_Controller_Router_Route_Static',
                                    'route' => '',
                                    'defaults' => array(
                                        'controller' => 'supplier',
                                        'action' => 'errands'
                                    )
                                ) 
                            )
                        )
                    ),
                ),
                'carrier' => array(
                    'type' => 'Zend_Controller_Router_Route_Static',
                    'route' => 'carrier',
                    'chains' => array(
                        'finalize' => array(
                            'type' => 'Zend_Controller_Router_Route',
                            'route' => 'finalizu/:id',
                            'defaults' => array(
                                'controller' => 'carrier',
                                'action' => 'finalize'
                            )
                        ),
                        'bid' => array(
                            'type' => 'Zend_Controller_Router_Route',
                            'route' => 'bid/:id',
                            'defaults' => array(
                                'controller' => 'carrier',
                                'action' => 'bid'
                            )
                        ),
                        'bids' => array(
                            'type' => 'Zend_Controller_Router_Route_Static',
                            'route' => 'bids',
                            'defaults' => array(
                                'controller' => 'carrier',
                                'action' => 'bids'
                            )
                        ),
                        'index' => array(
                            'type' => 'Zend_Controller_Router_Route_Static',
                            'route' => '',
                            'defaults' => array(
                                'controller' => 'carrier',
                                'action' => 'index'
                            )
                        ) 
                    )
                ),
                'index' => array(
                    'type' => 'Zend_Controller_Router_Route_Static',
                    'route' => '',
                    'defaults' => array(
                        'controller' => 'index',
                        'action' => 'index'
                    )

                ),
                'reset' => array(
                    'type' => 'Zend_Controller_Router_Route_Static',
                    'route' => 'reset-prototype-data',
                    'defaults' => array(
                        'controller' => 'index',
                        'action' => 'reset'
                    )

                )
            )
        )
    )
);
