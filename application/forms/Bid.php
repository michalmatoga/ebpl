<?php

class Form_Bid extends Twitter_Bootstrap_Form_Horizontal
{
    public function init()
    {
        $this->setMethod('POST'); 

        $element = new Zend_Form_Element_Text('price');
        $element->setLabel('Oferowana cena')->setRequired();
        $this->addElement($element);

        $element = new Zend_Form_Element_Submit('submit');
        $element->setAttrib('class', 'btn btn-primary')->setLabel('Wyślij');
        $this->addElement($element);

        $this->setElementDecorators(array(
            array('FieldSize'),
            array('ViewHelper'),
            array('Addon'),
            array('ElementErrors'),
            array('Description', array('tag' => 'p', 'class' => 'help-block')),
            array('HtmlTag', array('tag' => 'div', 'class' => 'controls')),
            array('Label', array('class' => 'control-label')),
            array('Wrapper')
        ));
    }
}

