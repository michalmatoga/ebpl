<?php

class Form_Errand extends Twitter_Bootstrap_Form_Horizontal
{
    public function init()
    {
        $this->setMethod('POST'); 

        $element = new Zend_Form_Element_Text('date_start');
        $element->setLabel('Data i czas załadunku')->setRequired();
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('address_start');
        $element->setLabel('Adres załadunku')->setRequired();
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('date_finish');
        $element->setLabel('Data i czas dostawy')->setRequired();
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('address_finish');
        $element->setLabel('Adres dostawy')->setRequired();
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('mass');
        $element->setLabel('Masa towaru [kg]')->setRequired();
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('desc');
        $element->setLabel('Opis');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('price');
        $element->setLabel('Cena [PLN]')->setRequired();
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('carrier');
        $element->setLabel('Przewoźnik')->setRequired()
            ->setMultiOptions(array(
                'Giełda',
                'Przewoźnik 1',
                'Przewoźnik 2',
                'Przewoźnik N',
            ))->setAttrib('disable', array(1,2,3));
        $this->addElement($element);

        $element = new Zend_Form_Element_Submit('submit');
        $element->setAttrib('class', 'btn btn-primary')->setLabel('Wyślij');
        $this->addElement($element);

        $this->setElementDecorators(array(
            array('FieldSize'),
            array('ViewHelper'),
            array('Addon'),
            array('ElementErrors'),
            array('Description', array('tag' => 'p', 'class' => 'help-block')),
            array('HtmlTag', array('tag' => 'div', 'class' => 'controls')),
            array('Label', array('class' => 'control-label')),
            array('Wrapper')
        ));
    }
}

