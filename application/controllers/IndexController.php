<?php
/**
 * @author Michał Matoga <michalmatoga@gmail.com>
 */

class IndexController extends Zend_Controller_Action
{
    public function indexAction(){
    
    }

    public function resetAction(){
        $adapter = Zend_Db_Table::getDefaultAdapter();
        $adapter->query('TRUNCATE `errands`');
        $adapter->query('TRUNCATE `bids`');
        exit;
    }
}
