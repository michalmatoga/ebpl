<?php
/**
 * @author Michał Matoga <michalmatoga@gmail.com>
 */

class CarrierController extends Zend_Controller_Action
{
    public function init()
    {
        $this->getHelper('Layout')->setLayout('carrier');
    }
    public function indexAction()
    {
        $model = new Model_Errands();
        $data = $model->fetchAll(array('status = 0'));
        $this->view->errands = $data->toArray();
    
    }
    public function bidsAction()
    {
        $model = new Model_Bids();
        $this->view->bids = $model->getBids();
    }
    public function bidAction()
    {
        $request = $this->getRequest();
        $form = new Form_Bid();
        if ($request->isPost()){
            if ($form->isValid($request->getPost())){
                $model = new Model_Bids();
                $data = $form->getValues();
                $data['errand_id'] = $request->getParam('id');
                $model->insert($data);
                $this->getHelper('FlashMessenger')
                    ->addMessage(array('type' => 'success', 'content' => 'Złożono ofertę'));
                return $this->getHelper('Redirector')
                    ->goToUrl($this->view->url());
            } 
        }
        $this->view->form = $form;
        $formErrand = new Form_Errand();
        $model = new Model_Errands();
        $data = $model->fetchRow(array('id =?' => $request->getParam('id')));
        $formErrand->populate($data->toArray());
        $formErrand->removeElement('submit');
        $formErrand->removeElement('carrier');
        foreach($formErrand->getElements() as $element){
            $element->setAttrib('readonly', 'readonly'); 
        }
        $this->view->formErrand = $formErrand;
    
    }

    public function finalizeAction()
    {
        $model = new Model_Bids();
        $model->update(array('status' => '2'),
            array( 'status = 1', 'id =?' => $this->getRequest()->getParam('id')));
                $this->getHelper('FlashMessenger')
                    ->addMessage(array('type' => 'success', 'content' => 'Status oferty został zmieniony'));
                return $this->getHelper('Redirector')
                    ->goToUrl($this->view->url(array(), 'carrier-bids'));
    
    }
}
