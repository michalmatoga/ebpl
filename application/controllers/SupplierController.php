<?php
/**
 * @author Michał Matoga <michalmatoga@gmail.com>
 */

class SupplierController extends Zend_Controller_Action
{
    public function init()
    {
        $this->getHelper('Layout')->setLayout('supplier');
    }

    public function errandsAction()
    {
        $model = new Model_Errands();
        $data = $model->fetchAll();
        $this->view->errands = $data->toArray();
    }

    public function newAction()
    {
        $request = $this->getRequest();
        $form = new Form_Errand();
        if ($request->isPost()){
            if ($form->isValid($request->getPost())){
                $model = new Model_Errands();
                $model->insert($form->getValues());
                $this->getHelper('FlashMessenger')
                    ->addMessage(array('type' => 'success', 'content' => 'Dodano zlecenie'));
                return $this->getHelper('Redirector')
                    ->goToUrl($this->view->url(array(), 'supplier-errands-errands'));
            } 
        }
        $this->view->form = $form;
    }

    public function showAction()
    {
        $request = $this->getRequest();
        $form = new Form_Errand();
        $model = new Model_Errands();
        $data = $model->fetchRow(array('id =?' => $request->getParam('id')));
        $form->populate($data->toArray());
        $form->removeElement('submit');
        foreach($form->getElements() as $element){
            $element->setAttrib('readonly', 'readonly'); 
        }
        $this->view->form = $form;
        $model = new Model_Bids();
        $bids = $model->fetchAll(array('errand_id =?'
            => $request->getParam('id'), 'status <> -1'));
        $this->view->bids = $bids->toArray();
    }

    public function acceptBidAction()
    {
        $model = new Model_Bids();    
        $model->update(array('status' => 1),
            array('id =?' => $this->getRequest()->getParam('id')));
        $model->update(array('status' => -1),
            array('id <>?' => $this->getRequest()->getParam('id'),
            'errand_id =?' => $this->getRequest()->getParam('errandid')
        ));
        $model = new Model_Errands();    
        $model->update(array('status' => 1),
            array('id =?' => $this->getRequest()->getParam('errandid')));
        $this->getHelper('FlashMessenger')
            ->addMessage(array('type' => 'success', 
            'content' => 'Oferta została zaakceptowana'));
        return $this->getHelper('Redirector')
            ->goToUrl($this->view->url(array(), 'supplier-errands-errands'));
    }
}
