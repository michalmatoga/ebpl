<?php
/**
 * @author Michał Matoga <michalmatoga@gmail.com>
 */

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initAutoload()
    {
        $resourceLoader = new Zend_Loader_Autoloader_Resource(array(
            'basePath' => APPLICATION_PATH,
            'namespace' => '',
        ));

        $resourceLoader->addResourceType('model', 'models/', 'Model');
        $resourceLoader->addResourceType('form', 'forms/', 'Form');

        return $resourceLoader;
    }
    protected function _initViewSettings()
    {
        $view = $this->bootstrap('view')->getResource('view');
        $view->addHelperPath('App/View/Helper', 'App_View_Helper');
    }
}
