<?php

class Model_Bids extends Zend_Db_Table_Abstract 
{
    protected $_name = 'bids';

    public function getBids()
    {
        $select = $this->select()->setIntegrityCheck(false)
            ->from(array('b' => $this->_name), array('*'))
            ->join(array('e' => 'errands'), 'b.errand_id = e.id', array('date_start', 'date_finish'));
        $bids = $this->fetchAll($select)->toArray();
        $output = array('done' => array(), 'pending' => array(), 'won' => array(), 'lost' => array());
        foreach($bids as $bid){
            switch($bid['status']){
                case 0: $output['pending'][] = $bid; break; 
                case -1: $output['lost'][] = $bid; break; 
                case 1: $output['won'][] = $bid; break; 
                case 2: $output['done'][] = $bid; break; 
            } 
        }
        return $output;
    }
}
